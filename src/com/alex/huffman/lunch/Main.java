package com.alex.huffman.lunch;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Scanner;

import com.alex.huffman.impl.Node;

public class Main {
	
	private static int treeHeight = 0;
	private static int encondingLength = 0;
	private static Map<Character, Integer> symbolCount = new HashMap<>();
	
	public static void main(String args[]) {
		
		Scanner scanner = new Scanner(System.in);
		String data = scanner.nextLine();
		scanner.close();
		
		/*
		 * Step 1 : Counting Frequencies
		 */
		countingFreqeuncies(data);
		
		/*
		 * Step 2 : Building Priority Queue, Nodes and root
		 */
		PriorityQueue<Node> pQueue = new PriorityQueue<>();
		buildingPriorityQueue(pQueue);
		buildingNodes(pQueue);
		Node root = pQueue.poll();
			
		/*
		 * Step 3 : Finding height of root and length of code
		 */
		findTreeHeight(root, 0);
		findEncondingLength(root, 0);	
		System.out.println("treeHeight : " + treeHeight);
		System.out.println("encondingLength : " + encondingLength);	 
	}

	private static void countingFreqeuncies(String data) {
		for(int i=0; i<data.length();i++) {
			if(!symbolCount.containsKey(data.charAt(i))) 
			{
				symbolCount.put(data.charAt(i), 1);
			} else
			{
				symbolCount.put(data.charAt(i), symbolCount.get(data.charAt(i)) + 1);
			}
		}
	}
	
	private static void buildingPriorityQueue(PriorityQueue<Node> pQueue) {
		for(Map.Entry<Character, Integer> entry : symbolCount.entrySet()) {
			Node currentNode = new Node(entry.getKey(), entry.getValue());
			pQueue.offer(currentNode);
		}
	}
	
	private static void buildingNodes(PriorityQueue<Node> pQueue) {
		while(pQueue.size() > 1) 
		{
			Node leftNode = pQueue.poll();
			Node rightNode = pQueue.poll();
			
			Node parentNode = new Node(leftNode.getCount() + rightNode.getCount());
			
			parentNode.setLeft(leftNode);
			parentNode.setRight(rightNode);
			pQueue.offer(parentNode);
		}
	}
	
	public static void findTreeHeight(Node currentNode, int currentHeight) {

		if(currentNode == null) return ;
		
		findTreeHeight(currentNode.getLeft(), currentHeight + 1);
		findTreeHeight(currentNode.getRight(), currentHeight + 1);
		
		if(currentHeight > treeHeight) treeHeight = currentHeight;
		
		return ;
	}
	
	public static void findEncondingLength(Node currentNode, int currentHeight) {
		
		if(currentNode == null) return ;
		
		findEncondingLength(currentNode.getLeft(), currentHeight + 1);
		findEncondingLength(currentNode.getRight(), currentHeight + 1);
		
		char currentSymbol = currentNode.getSymbol();
		if(currentSymbol != '\u0000') encondingLength += currentHeight*symbolCount.get(currentSymbol);
	
		return ;
	}
}









