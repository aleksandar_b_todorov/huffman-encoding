package com.alex.huffman.impl;

public class Node implements Comparable<Node>{
	
	private char symbol = '\u0000';
	private int count = 0;
	
	private Node left = null;
	private Node right= null;
	
	public Node(char symbol, int count) {
        this.count = count;
        this.symbol = symbol;
    }
	
	public Node(int count){
		 this.count = count;;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
	

	public char getSymbol() {
		return symbol;
	}

	public void setSymbol(char symbol) {
		this.symbol = symbol;
	}

	public Node getLeft() {
		return left;
	}

	public void setLeft(Node left) {
		this.left = left;
	}

	public Node getRight() {
		return right;
	}

	public void setRight(Node right) {
		this.right = right;
	}
	
	@Override
	public int compareTo(Node node) {
		return this.count - node.getCount();
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		final int PRIME = 31;
		return ((int)this.symbol*PRIME + this.count)*PRIME;
	}
}
